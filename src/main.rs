use anyhow::Result;

use std::io::{self, Read, Write};
use std::path::PathBuf;
use std::process::{Command, Stdio};

fn main() -> Result<()> {
    let mut args = std::env::args();
    args.next().unwrap();

    let msg = "FILENAME is required";
    let file = args.next().expect(msg);
    let mut data = String::new();

    if &file == "-" {
        io::stdin().read_to_string(&mut data)?;
    } else {
        let path = PathBuf::from(&file);
        if !path.exists() {
            eprintln!("{}: does not exist", msg);
            std::process::exit(1);
        }
        data = std::fs::read_to_string(path)?;
    }

    let doc: toml::Value = toml::from_str(&data)?;
    let jq_args: Vec<String> = args.collect();

    let mut jq = Command::new("jq")
        .args(&jq_args)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()?;

    {
        let jq_in = jq.stdin.as_mut().expect("failed to get stdin for jq");
        jq_in.write_all(&serde_json::to_vec(&doc)?)?;
    }

    let jq_out = jq.wait_with_output()?;
    match serde_json::from_slice::<serde_json::Value>(&jq_out.stdout) {
        Ok(json) => print!("{}", toml::to_string_pretty(&json)?),
        Err(_) => print!("{}", String::from_utf8(jq_out.stdout)?),
    }

    Ok(())
}
